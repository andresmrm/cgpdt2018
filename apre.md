---
title: Tecnologia, Política e iniciativas com Dados Governamentais Abertos
subtitle: Congresso do Grupo de Pesquisa Democracia e Tecnologia
date: 27/02/2018
lang: pt-BR
author: Andrés M. R. Martano
theme: solarized
---


# Observar e Controlar

# Vigilância antiga
- Feita por pessoas
- Repressiva
- Custosa

# Vigilância 2.0
- Automatizada
- Gera lucro
- Larga escala
- Indiscriminada
- Sedutora

# Facebook

- Observar: coleta massiva de dados
- Controlar: mensagens direcionadas

---

[Artigo](http://www.pnas.org/content/pnas/early/2015/01/07/1418680112.full.pdf) de 2015 afirma que *likes* permitem prever o seu comportamento às vezes melhor do que você mesmo.

Destaca-se que o modelo deles não era otimizado para previsão de comportamento, mas sim para traços de personalidade.

---

![](imgs/previsao1.png){height=500px}

---

![](imgs/previsao2.png){height=500px}

---

[Experimento de humor](http://www.theatlantic.com/technology/archive/2014/06/everything-we-know-about-facebooks-secret-mood-manipulation-experiment/373648/): em 2012 o Facebook manipula os conteúdos exibidos para cerca de 700mil pessoas, concluindo que conseguiam assim influenciar o humor delas. Só sabemos disso pois publicaram um artigo em 2014 contando a "façanha".

Alegaram que quando as pessoas aceitam os termos de uso autorizam esse tipo de experimento.

---

[Disposição de influenciar eleições](https://newrepublic.com/article/117878/information-fiduciary-solution-facebook-digital-gerrymandering): em 2010 o Facebook mostrou mensagens de incentivo ao voto para **60 milhões** de pessoas nos EUA.
O [artigo](http://fowler.ucsd.edu/massive_turnout.pdf) deles de 2012 conclui que o experimento levou 340mil pessoas a votarem.

Apesar de um resultado modesto, escolhendo o grupo com base em preferências políticas, é capaz de definir uma eleição. Isso em 2010.

---

- Vício: ex-executivos da plataforma falam sobre com foi desenvolvida para explorar necessidades psicológicas das pessoas. [[1](https://www.theguardian.com/technology/2017/oct/05/smartphone-addiction-silicon-valley-dystopia), [2](https://www.nexojornal.com.br/expresso/2017/12/12/As-cr%C3%ADticas-de-um-ex-executivo-do-Facebook-%C3%A0-rede-social)]

- [Efeito rede](https://en.wikipedia.org/wiki/Network_effect): quanto maior uma rede, mais valor ela acumula. [Sociedade refém da visibilidade que ela controla.](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2594754)

---

[![](imgs/facebook.jpg){height=500px}](https://www.flickr.com/photos/cau_napoli/4438809405/in/photostream/)

---

Muitos desses pontos não são exclusivos do Facebook, mas comuns às grandes empresas de tecnologia.

Não se trata de avaliar se a empresa é "boa" ou "má", mas sim de concentração de **poder**.

# Capitalismo de Vigilância

[Analisando o discurso](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2594754) de economista chefe do Google (Hal Varian), Shoshana Zuboff discute a nova etapa do Capitalismo em que nos encontramos.

---

Poder passa dos proprietários dos meios de produção para os proprietários dos meios de **modificação comportamental**.

---

Vigilância não acaba com a **privacidade**, mas a **concentra**: a população perde sua privacidade, mas a [elite mantém sua privacidade](https://www.eff.org/deeplinks/2009/12/google-ceo-eric-schmidt-dismisses-privacy) e segredos (inclusive sobre a capacidade de vigilância massiva).

---

Empresas de tecnologia **abusam** do fato de atuarem em um **campo ainda não regulamentado**, explorando o máximo possível, quando encontram resistências usam seus exércitos de advogados. Quando são multadas, geralmente são valores insignificantes para elas. Exemplos: Google Street View, Uber, experimentos de manipulação do Facebook.

---

- Pessoas seduzidas pelas facilidades que as ferramentas fornecem.
- Necessidade de aceitar a invasão de privacidade ou correr o risco de ser menos produtiva ou mesmo excluída da vida em sociedade.

# Controle pelo prazer
[Zinep](http://firstmonday.org/ojs/index.php/fm/article/view/4901/4097), [Byung-Chul](https://brasil.elpais.com/brasil/2014/09/22/opinion/1411396771_691913.html): prazer ao invés da dor, mais eficiência e produtividade.

Mistura de 1984 com Admirável Mundo Novo.

# O que fazer?

# Respostas tecnológicas
- Criptografia ponta-a-ponta
- Software Livre
- [Redes Federadas](https://www.eff.org/deeplinks/2011/03/introduction-distributed-social-network) (como [Diaspora](https://diasporafoundation.org/) e muitas [outras](https://en.wikipedia.org/wiki/Comparison_of_software_and_protocols_for_distributed_social_networking))
- *Container* pessoal de dados? (como [Databox Project](https://www.databoxproject.uk/))

# Blockchain?

Falta um debate mais crítico sobre a tecnologia.
Nem sempre ajuda a resolver o problema, e às vezes serve de cortina de fumaça escondendo as reais relações de poder, como a [concentração do poder de mineração](https://blockchain.info/pools).

- Bitcoin (Varoufakis: [A Fantasia do Dinheiro Apolítico](http://autonomialiteraria.com.br/varoufakis-bitcoin-e-a-bolha-perfeita-mas-o-blockchain-e-uma-solucao-excepcional/))
- Smart Contracts ([Ethereum: Freenet or Skynet?](https://cyber.harvard.edu/events/luncheon/2014/04/difilippi))

Geralmente não é necessário Blockchain para tecnologias descentralizadas.

# Há jeitos de burlar a tecnologia

![](imgs/xkcd.jpg){height=500px}

---

# Política é importante e inevitável

Yasha Levine [entrevista](https://thebaffler.com/salvos/the-crypto-keepers-levine) criador do Telegram (Pavel Durov), falam sobre a impossibilidade de desenvolver tecnologias seguras em um país como os EUA, por conta do assédio das agências governamentais.

Criticando Snowden, Yasha defende que tecnologia só não basta, legislação também é necessária.


<!-- # Legislação -->



---

- Legislação sobre proteção dos dados. Como [GDPR](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2972855) e [PL 5276/2016](https://www.cartacapital.com.br/blogs/intervozes/por-que-precisamos-ja-de-uma-lei-de-protecao-de-dados-pessoais).
- Transparência e gestão democrática das plataformas digitais (são privadas, mas assumem caráter público).
- Necessidade de construir alianças internacionais sobre o tema, inclusive para pressionar por legislações adequadas.
- Posição do Brasil sobre o assunto é relevante e impacta internacionalmente.

---

Formação em computação: cursos de graduação não estão preparados para os problemas éticos e sociais que a computação cada vez mais irá encontrar.


# Busca por soluções que aliem tecnologia e política

# Cuidando do Meu Bairro

Plataforma, desenvolvida desde 2011 pelo [Colab-USP](https://colab.each.usp.br), que mapeia o orçamento da cidade de São Paulo-SP, facilitando inclusive o envio de pedidos de acesso à informação.

[cuidando.vc](https://cuidando.vc)

---

- Usa dados de execução orçamentária disponibilizados pela prefeitura
- Parceria com o [Observatório Social de São Paulo](http://saopaulo.osbrasil.org.br/)
- Dezenas de oficinas (Escola de Cidadania, Escola do Parlamento, Agente de Governo Aberto, Observatório Social)
- Prêmio de Educação Fiscal 2016 (FEBRAFITE)

---

![](imgs/cuidando.png){height=500px}

---

![](imgs/cuidando2.png){height=500px}

# Agentes de Governo Aberto

[Iniciativa da prefeitura](https://archive.org/stream/SPABERTA/LIVRO_SP_ABERTA#page/n157/mode/2up) de São Paulo, criada em 2015, que fornece bolsas para oficinas na temática de Governo Aberto.

Proposta de popularizar ideias do Governo Aberto.

---

- 48 oficinas por ciclo (6 meses)
- Eixos: 
  - Mapeamento Colaborativo
  - Tecnologia Aberta e Colaborativa
  - Transparência e Dados Abertos
  - Comunicação em Rede
- As próprias pessoas propõem oficinas
- Oficinas espalhadas por toda a cidade
- Desafio de casar oferta e demanda

# Monitorando a Cidade

Plataforma e metodologia para criação de [campanhas cívicas](http://promisetracker.org) de coleta de dados, desenvolvido pelo [Center for Civic Media (MIT)](https://civic.mit.edu).

[monitorandoacidade.org](http://monitorandoacidade.org)

---

- Surge da vontade de monitorar o Programa de Metas municipal: [De Olho nas Metas](https://deolhonasmetas.org.br)
- Percebem a importância de deixar as pessoas escolherem o que monitorar

---

- Site para criação de campanhas
- Coleta de dados via celular
- Relatório automático
- Metodologia participativa
- Parceria com Colab-USP
- Experiências de monitoramento da merenda em escolas públicas no Pará [[1](https://colab-usp.github.io/monitorando-relatorio-fase2/),[2](http://bit.ly/monitorando-sumario)]

# Radar Parlamentar

Plataforma desenvolvida, principalmente, por equipe do [PoliGNU-USP](https://polignu.org/) desde 2012, que gera visualizações e análises sobre as casas legislativas.

[radarparlamentar.polignu.org](http://radarparlamentar.polignu.org)

---

- Gráficos sobre:
  - Senado
  - Câmara dos Deputados
  - Câmara Municipal de São Paulo
- [Análise quantitativa](https://www.revistas.usp.br/leviathan/article/view/143408) sobre semelhanças entre partidos políticos com base nas votações das casas legislativas
- Parceria com disciplina da UNB (disciplina sobre manutenção de software)
- Participação em Hackathona da Câmara dos Deputados (2013) [[1](http://blog.hsvab.eng.br/2013/11/01/a-experiencia-deste-hackathon-na-camara-dos-deputados/),[2](http://www2.camara.leg.br/a-camara/programas-institucionais/programas-anteriores/hackathon/2014/deputados)]

---

![](imgs/radar.png){height=500px}

---

![](imgs/mulheres.png){height=500px}

# Diário Livre

Plataforma fruto de uma parceria entre Colab-USP e Prefeitura de São Paulo (2014), para publicar o Diário Oficial da Cidade de São Paulo em formato aberto.

[devcolab.each.usp.br/do](https://devcolab.each.usp.br/do)

---

- Proposta inicial envolvia publicação conjunta de várias outras bases
- Acumula cerca de 300mil acessos
- Publicação em HTML, XML e JSON, além de download integral da base
- [Prêmio do Congresso de Informática e Inovação na Gestão Pública 2015 (CONIP)](http://each.uspnet.usp.br/site/conteudo-imprensa-noticia.php?noticia=1988)
- Dificuldade de internalização
- Direito ao esquecimento?

# Fim

Obrigado!

Link para essa apresentação:

[andresmrm.gitlab.io/cgpdt2018](https://andresmrm.gitlab.io/cgpdt2018)

Grupo sobre Direito Digital:

[digitalrights.cc](http://digitalrights.cc)
