# Apresentação

Apresentação para o Congresso do Grupo de Pesquisa Democracia e Tecnologia.

[Apresentação](https://andresmrm.gitlab.io/cgpdt2018)

[Página comum](https://andresmrm.gitlab.io/cgpdt2018/pagina.html)

# Compilar

Precisa de pandoc.

Para compilar uma vez:

	./run.sh
    
Para compilar quando `apre.md` mudar:

    echo apre.md | entr ./run.sh
